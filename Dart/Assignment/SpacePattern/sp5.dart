

import "dart:io";

 void main() {

	int row = int.parse(stdin.readLineSync()!);
	int num = 1;

	for (int i=1;i<=row;i++){
		
		for(int sp =row-i;sp>0;sp--){
			
			stdout.write("	");
		}
		for (int j=1;j<=i;j++){
			
			stdout.write("${num*num}	");
			num++;
		}
		stdout.writeln();
	}

}
