

 import "dart:io";

void main() {

	int row = int.parse(stdin.readLineSync()!);
	int num = row;


	for (int i=1;i<=row;i++){
		
		for(int sp=i;sp>1;sp--){
		
			stdout.write("	");
		}
		for(int j=i;j<=row;j++){
		
			stdout.write("$num	");
		}
		
		num--;
		stdout.writeln();
	}
}
