

void main() {

	double z = 18.5;

	if(z==18.5)
		print("Underweight");
	else if(z>=18.5 && z<=24.9)
		print("Normal");
	else if(z>=25.0 && z<=29.9) 
		print("Overweight");
	else if(z>=30.0 && z<=34.9)
		print("Obese");
	
	else
		print("Extreme Obese"); 
}
