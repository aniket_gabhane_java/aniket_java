


import "dart:io";

void main() {

	int row = int.parse(stdin.readLineSync()!);

	for(int i=1;i<=row;i++){
		
		int num =1;
		for(int sp=i;sp<row;sp++){
			
			stdout.write("	");

		}
		
		for(int j=1;j<=i*2-1;j++){
				
			if(j<i){
				
				stdout.write("${num++}	");
			}

				
			else{
				stdout.write("${num--}	");
			}
			

			
		}
		
	
		print("");
	}
}
