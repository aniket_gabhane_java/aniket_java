
// four method to write the Obj 

class Employee {

	int empId = 12;
	String empName = "Vijay";
	double empSal = 24.67;
	

	Employee() {

	//this. empId = empId;
}
	void Demo () {

		print("EmpId = $empId");
		print('EmpName = $empName');
		print('EmpSal = $empSal');
	}
}
void main() {

	Employee obj = new Employee();  //type1
	obj.Demo();

	Employee obj2 =  Employee();    //type2
	obj2.Demo();

	new Employee().Demo();           //type3


	Employee().Demo();              //type4

}
