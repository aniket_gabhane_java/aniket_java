

class Employee {
	
	int? empId ;
	String? empName;
	double? empSal;

	Employee(int empId,String empName,double empSal) {

		this.empId = empId;
		this.empName = empName;
		this.empSal = empSal;
	}
	
	void empInfo() {
		print("empId:$empId");
		print("EmpName:$empName");
		print("EmpSal:$empSal");
	}
}
void main() {

	Employee obj = new Employee(12,"Aniket",57.99);
	obj.empInfo();

}
