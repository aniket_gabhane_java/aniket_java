
// git null value

class Employee {
	
	//Propertties
	int? empId ;
	String? empName;
	double? empSal;

	void Emp () {
	
		print("Empid : $empId");
		print("EmpName : $empName");
		print("empSal : $empSal");
	} 

}

void main() {

	Employee obj = new Employee();
	obj.Emp();

	obj.empId = 12;
	obj.empName = "Aniket";
	obj.empSal = 23.34;

	obj.Emp();
}
