



class Demo {

	Demo(){
	
		print("In Constructor");
	}

	Demo.parse(int a,String b,double c) {
		
		print("In parametrized Constructor");
	}

}

void main() {
	
	Demo obj = new Demo();

	Demo obj2 = Demo.parse(12,"Vaijanth",45.45);

}
