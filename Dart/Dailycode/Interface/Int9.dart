


class Parent1 {

	Parent1() {
		print("Contructor parent1");
	}
}
class Parent2 {
	
	Parent2() {
		print("Constructor parent2");
	}
}
class Demo extends Parent1 {

	Demo() {

		print("Demo Constructor");
	}
}
class Child implements Parent1 {

	Child():super() {
		
		print("Child Constructor");
	}
}

void main() {

	Child obj = Child ();
}
