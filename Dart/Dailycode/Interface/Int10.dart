
abstract class Parent {

	int num = 110;

	_fun() {
		
		print("In fun");
	}

}

class Child implements Parent {

	int num = 10;
	_fun() {
		print("In child");
	}
}

void main() {

	Child obj = Child();
	obj._fun();
}
