
//To understand the flow of current

abstract class Parent1 {

	void fun () {

		print("In fun Parent1");
	}
}
abstract class Parent2 {

	void run () {

		print("In run Parent2");
	}
}

class Child extends Parent1 {

	void fun () {
		print("In fun Child");
	}
}

class BOSS extends Child implements Parent1,Parent2 {

	void fun() {

		print("Boss fun");
	}

	void run() {

		print("Boss run");
	}
}

void main() {

	BOSS obj = BOSS();
        obj.fun();
	obj.run();	
}
