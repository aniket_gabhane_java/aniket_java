


abstract class Parent1 {

		void fun ();
}
abstract class Parent2 {

		void run () ;
}

class Child extends Parent2 {

 		void run() {
			
			print("In run");
	}
}

class Mother extends Child implements Parent1 {

		void fun () {

			print("In fun");
		}
}


void main() {

	Mother obj = Mother();
	obj.run();
	obj.fun();

}
