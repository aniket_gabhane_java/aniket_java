//mixin does not have the Constructor

mixin Parent {

	int x = 10;

}

class Child extends Parent {

	Child() {

		print("Constructor Child");
		print(x);
	}
}

void main() {

	Child obj = Child();
}
