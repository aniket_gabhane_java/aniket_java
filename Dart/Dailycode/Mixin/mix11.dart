


mixin Parent1 {

	int x = 10;
	void fun() {
		
		print("In fun-parent");
	}
}
mixin Parent2 {

	int y = 20;
	void run() {

		print("In fun-parent2");
	}
}

class Child with Parent1,Parent2{

	
}

void main () {

	Child obj = Child() ;
	obj.fun();
	print(obj.x);
	obj.run();
	print(obj.y);
}
