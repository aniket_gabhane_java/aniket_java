


 mixin Parent {

	int x = 20;
	void fun() {
		print("In fun");
	}
}

class Child with Parent{

	
}

void main() {

	Child obj = Child();
	obj.fun();
	print(obj.x);
}
