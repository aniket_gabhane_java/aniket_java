

class Parent {

	int x = 10;
	Parent() {

		print("Constructor Parent");
	}
}

class Child extends Parent {

	Child() {
		
		print("Constructor Child");
 	}
}

void main (){

      Child obj = Child();
}
