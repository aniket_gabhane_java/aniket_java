



class Parent {

	int x = 10;
	Parent() {

		print("Constructor Parent");
	}
}

mixin Demo {

	int y = 20;
	Demo() {

		print("Mixin Constructor");
	}
}

void main () {

	Demo obj =  Demo();
}
