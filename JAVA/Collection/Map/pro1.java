


import java.util.*;
class PlayerInfo implements Compariable <PlayerInfo> {

	
		int jerNo;
		String pName;

		PlayerInfo(int jerNo,String pName) {
		
			this.jerNo = jerNo;
			this.pName = pName;
		}

		public int compareTo(PlayerInfo obj) {
		
			return pName.compareTo(obj.pName);
		}

		public String toString() {
		
			return "{"+jerNo+";"+pName+"}";
		}
	}

	class Treemap {
	
		public static void main(String[] args) {
		
			WeakHashMap whm = new WeakHashMap();

			Demo obj1 = new Demo();
			Demo obj2 = new Demo();
			Demo obj3 = new Demo();

			whm.put(obj1,"Abc");
			whm.put(obj2,"xyz");
			whm.put(obj3,"mno");

			System.out.println(whm);

			obj1 = null;
			obj3 = null;

                        System.gc();

			System.out.println(whm);

		}
	}

