


import java.util.*;
class PlayerInfo implements Comparable <PlayerInfo> {

	
		int jerNo;
		String pName;

		PlayerInfo(int jerNo,String pName) {
		
			this.jerNo = jerNo;
			this.pName = pName;
		}

		public int compareTo(PlayerInfo obj) {
		
			return pName.compareTo(obj.pName);
		}

		public String toString() {
		
			return "{"+jerNo+";"+pName+"}";
		}
	}

	class Treemap1 {
	
		public static void main(String[] args) {
		
			TreeMap tm = new TreeMap();

			PlayerInfo obj1 = new PlayerInfo(1,"Aniket");
			PlayerInfo obj2 = new PlayerInfo(8,"Aniket");
			PlayerInfo obj3 = new PlayerInfo(6,"Aniket");
			PlayerInfo obj4 = new PlayerInfo(15,"Aniket");

			tm.put(obj1,"Abc");
			tm.put(obj2,"xyz");
			tm.put(obj3,"mno");

			System.out.println(tm);
			System.out.println(tm);
			System.out.println(tm);

	

		}
	}

