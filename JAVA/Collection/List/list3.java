
//List is Interface
//In the list doublicate element is allow
//


import java.util.*;
class ListDemo {

	public static void main(String[] args) {
	
		List <Object> ls = new ArrayList <Object> (7);

		//add method

		ls.add(10);
		ls.add(20);
		ls.add(30);
		ls.add(40);
		ls.add(50);
		ls.add(60);
		ls.add("Aniket");
		
		System.out.println(ls);                      // [10,20,30,40,50,60,Aniket]

		//add at that 6 arryalist place       
		ls.add(6,"Gabhane");
		System.out.println(ls);                     // [10,20,30,40,50,60,Gabhane,Aniket]

		//contains
		System.out.println(ls.contains("Aniket"));  //true

		//get
		System.out.println(ls.get(7));              //[Aniket]

		//isEmpty
		System.out.println(ls.isEmpty());           //false

		//remove
		System.out.println(ls.remove(6));            //Gabhane
		System.out.println(ls);

		//set
		ls.set(5,"Aradhya");     
		System.out.println(ls);                    // [10,20,30,40,50,Aradhaya,Aniket]

		//clear
		ls.clear();
		System.out.println(ls);
	}
}
