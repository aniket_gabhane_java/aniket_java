


import java.io.*;
class Dia1 {

	public static void main(String args[])throws IOException {
	
		System.out.println("Enter the Row");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)) ;
		int row =Integer.parseInt(br.readLine());

		for (int i=1;i<row*2;i++) {
		
			for (int sp=1;sp<=row-i;sp++) {
			
				System.out.print("	");
			}
			for (int j=1;j<=i*2-1;j++) {
			
				System.out.print("*	");
			}
			System.out.println();
		}
	}
}
