




class While8 {

	public static void main(String[] args) {
	
		int num = 256985;
		int rem = 1;
		int prod = 1;

		while(num>0) {
		
			rem=num%10;
			num/=10;
			if(rem%2==1) {
			        prod = rem*prod;
				System.out.print("");

			}
		}
		System.out.print("Product of odd digits:"+prod);
		System.out.println();

	}
}
