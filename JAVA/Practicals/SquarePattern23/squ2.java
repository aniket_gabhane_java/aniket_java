

import java.io.*;
class Squ2 {

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int row = Integer.parseInt(br.readLine());

		int num = row;
		for(int i=1;i<=row;i++) {
		
			for(int j=1;j<=row;j++) {
			
				if(row-i>=j) {
				
					System.out.print((char)(96+num)+"	");
				}
				else {
				
					System.out.print((char)(64+num)+"	");
				}
				num++;
			}
			System.out.println();
		}
	}
}
