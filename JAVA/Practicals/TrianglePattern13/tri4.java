



import java.util.Scanner;

class Tri4 {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		int row = sc.nextInt();

		int num = (row*row)-row-2;

		for(int i=1;i<=row;i++) {
		
			for(int j=1;j<=row-i+1;j++) {
			
				System.out.print((char)(64+num)+" ");
				num--;
			}
			System.out.println();
		}
	}
}
