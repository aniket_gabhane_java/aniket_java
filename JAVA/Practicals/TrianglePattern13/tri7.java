



import java.util.*;

class Tri7 {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
	       	int row = sc.nextInt();

		int num = row;
		for (int i=row;i>=1;i--) {

		         for(int j=1;j<=row-i+1;j++) {
			
				if(i%2==1) {
				
					System.out.print(num--+" ");
				}
				else{
				
					System.out.print((char)(96+num)+" ");
					num--;
				}
			}
			System.out.println();
		}
	}
}
