



import java.util.*;

class Tri5 {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		int row = sc.nextInt();

		for(int i=1;i<=row;i++) {

                        int num = 1;
			for(int j=1;j<=row-i+1;j++) {
			
				if(i%2==1) {
				
					System.out.print((char)(64+num)+" ");
				}
				else { 
					System.out.print((char)(96+num)+" ");
				}
				num++;
			}
			System.out.println();
		}
	}
}
