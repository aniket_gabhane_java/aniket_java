


import java.util.*;

class Tri1 {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		int row = sc.nextInt();
              
		for(int i=1;i<=row;i++) {
			
		        int num = i;
			for(int j=1;j<=row+1-i;j++) {
			
				System.out.print(num+" ");
				num++;
			}
			System.out.println();
		}
	}
}
