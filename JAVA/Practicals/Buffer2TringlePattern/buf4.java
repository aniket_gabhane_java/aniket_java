



import java.io.*;
class Buf4 {

	public static void main(String[] arsg)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows:");
		int row = Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++) {
		        int num =row;
			for(int j=1;j<=i;j++) {
			
				if(i%2==1) {
				
					System.out.print((char)(num+96)+" ");
					num--;
				}
				else {
				
					System.out.print((char)(num+64)+" ");
					num--;
				}
			}
			System.out.println();
		}
	}
}
