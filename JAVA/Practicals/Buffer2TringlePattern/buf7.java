



import java.io.*;
class Buf7 {

	public static void main(String[] arsg)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows:");
		int row = Integer.parseInt(br.readLine());
		int num = 1;
		for(int i=1;i<=row;i++) {
			for(int j=1;j<=i;j++) {
			
				if(j%2==0) {
				
					System.out.print((char)(num+96)+" ");
					num++;
				}
				else {
				
					System.out.print(i+" ");
				}
			}
			System.out.println();
		}
	}
}
