




class While4 {

	public static void main(String[] args) {
	
		int num = 214367689;
		int count1 = 0;
		int count2 = 0;
		int rem = 0;
		while (num>0) {
		
			rem=num%10;
			
			if (rem%2==0) {
			
				count1++;
				System.out.println(count1++ + " even number");
			}
			else {
			
				count2++;
				System.out.println(count2++ + "odd number");
			}
			num/=10;
		}
	}
}
