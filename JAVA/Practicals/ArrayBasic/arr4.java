


import java.io.*;

class Arr4 {

	public static void main(String[] args) throws IOException {
	
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		int num = Integer.parseInt(br.readLine());

		int arr[] =  {1,2,3,4,5,6,7,8,9,10};
		int sum = 0;
		for(int i=0;i<num;i++) {
		
			if(arr[i]%2==1)
			sum=sum+arr[i];
		}
			System.out.print(sum+" sum of odd number");
      }
}
