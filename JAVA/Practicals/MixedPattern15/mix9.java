



import java.io.*;

class Mix9 {

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
			int row = Integer.parseInt(br.readLine());

			int num = 3;
			for(int i=1;i<=row;i++) {

				for(int j=1;j<=row+1-i;j++) {
				
				       if (i%2==1) {
				       
					       System.out.print(j+ "  ");
				       }

			               else {
				       
					       System.out.print((char)(64+num)+"  ");
					       num--;
				       }	       
	         		}
				System.out.println();
			}
	}
}
