

import java.util.*;
class Space5{

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		System.out.print("enter the row:");
		int row = sc.nextInt();

		for(int i=1 ; i<=row;i++) {
		
			int num = row;
			for(int space = 1;space<=row-i;space++) {
			
				System.out.print("   ");
			}
			for(int j=1;j<=i;j++) {
			
				System.out.print(num+" ");
				num*=2;
			}
		System.out.println();
		}
		//System.out.println();
	}
}
