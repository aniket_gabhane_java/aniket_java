



import java.util.Scanner;

class Fact1 {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner (System.in);
		int row = sc.nextInt();

		for(int i=1;i<=row;i++) {
		
			if(row%i==0) 
				System.out.print(i+" ");
		}
		System.out.println();
	}
}
