

import java.io.*;
class Sq8 {

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int row = Integer.parseInt(br.readLine());

		int sun= 1;
		for(int i=1;i<=row;i++) {
		
			int num = row;
			for(int j=1;j<=row;j++) {
			
				if((i+j)%2==0) {
				
					System.out.print((char)(64+num)+"	");
					num--;
				}

				else {
				
					System.out.print("#"+"	");
				}
				sun++;
			}
			System.out.println();
		}
	}
}
