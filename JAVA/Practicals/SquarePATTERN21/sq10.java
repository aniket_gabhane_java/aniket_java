

import java.io.*;
class Sq10{

	public static void main(String[] args)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int row = Integer.parseInt(br.readLine());

		int sun= 1;
		for(int i=1;i<=row;i++) {
		
			int num = row;
			for(int j=1;j<=row;j++) {
			
				if(sun%2==0) {
				
					System.out.print((char)(64+num)+"	");
				}
				else if (i%2==0) {
				
					System.out.print((char)(64+num)+"	");
				}

				else {
				
					System.out.print(num+"	");
				}
				sun++;
				num--;
			}
			System.out.println();
		}
	}
}
