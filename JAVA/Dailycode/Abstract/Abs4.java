


abstract class Parent {

	abstract void Career();
}
class Child extends Parent{

	void Career() {
	
		System.out.println("IN Campus");
	}
}
class Demo1 {

	public static void main (String[] args) {
	
		Parent obj = new Child();
		obj.Career();
	}
}
