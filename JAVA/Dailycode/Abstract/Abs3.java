


abstract class Parent {

	abstract void career();
} 
class Child extends Parent {

	void career () {
	
		System.out.println("Doctor");
	}
}
class Client extends Child{

	public static void main(String[] args){
	
		Parent obj = new Child();
		obj.career();
	}
}
