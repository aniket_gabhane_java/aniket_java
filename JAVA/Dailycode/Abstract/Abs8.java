


abstract class Parent {

	abstract String marry();
	abstract String career();
	abstract String flat();
}
class Child extends Parent {

	String marry() {
		
		System.out.println("Aniket");
	}
}
class Demo3 extends Child{

	public static void main(String [] args){
		
			Parent obj = new Child();
			obj.marry();
	}
}
