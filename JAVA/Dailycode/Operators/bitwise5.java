

class Bit5 {
	public static void main(String[] args) {

		int x =8; //00001000
		int y =9; //00001001

		System.out.println(x&y); //00001000 8
		System.out.println(x|y); //00001001 9
		System.out.println(x^y); //00000001 1
	}
}
