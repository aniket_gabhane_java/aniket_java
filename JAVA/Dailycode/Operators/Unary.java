
// + - ++(incriment) --(decriment)
class una {
	public static void main(String[] args) {

		int x = 10;

		System.out.println(+x);  // 10
		System.out.println(-x);  //-10
		System.out.println(++x); //x=x+1 11
		System.out.println(--x); //x=x-1 10 because upeer x is 11
	}
}

